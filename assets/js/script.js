const canvas = document.getElementById("the_canvas");
const ctx = canvas.getContext("2d");

const WIDTH = canvas.width=  800;
const HEIGHT= canvas.height= 800;
let left_arrow = 37;
let up_arrow = 38;
let right_arrow=39;
let down_arrow=40;

//healthBar
var healthWidth = 200;
var healthHeight = 40;
var max = 100;
var val = 10;

let speed=3;
let score=0;

//slime 
let slimeTime=0;
let slimeFrame=15;

//player
let direction=0;
let playerTime=0;
let playerFrameRight=0;
let playerFrameLeft=59;
let playerFrameUp=0;
let playerFrameDown=0;


let dino = new Image();
dino.src = "assets/images/bigsheet.png";

let slimeSprite = new Image();
slimeSprite.src = "assets/images/slime.png";

let heart = new Image();
heart.src = "assets/images/heart.png";

function gameObject(spritesheet,x,y,width,height,scaleX, scaleY)
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.scaleX = scaleX * width;
    this.scaleY = scaleY * height;
}

let item = new gameObject(heart,150,150,32,32,1,1);

let player = new gameObject(dino,30,30,48,48,2,2);

let hitBox = new gameObject(dino,15,15,32,32,1,1);
let slimeHB = new gameObject(slimeSprite,15,15,32,32,1,1);

let slime = new gameObject(slimeSprite,300,300,32,32,2,2);


function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}


var dynamic = nipplejs.create({
    color: 'grey',
});
dynamic.on('added', function (evt, nipple) {
    //nipple.on('start move end dir plain', function (evt) {
    nipple.on('dir:up', function (evt, data) {
       //console.log("direction up");
       gamerInput = new GamerInput("Up");
       direction = 3;
    });
    nipple.on('dir:down', function (evt, data) {
        //console.log("direction down");
        gamerInput = new GamerInput("Down");
        direction = 4;
     });
     nipple.on('dir:left', function (evt, data) {
        //console.log("direction left");
        gamerInput = new GamerInput("Left");
        direction=2;
     });
     nipple.on('dir:right', function (evt, data) {
        //console.log("direction right");
        gamerInput = new GamerInput("Right");
        direction=1;
     });
     nipple.on('end', function (evt, data) {
        //console.log("mvmt stopped");
        gamerInput = new GamerInput("None");
        direction=0;
     });
});



// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input

//D pad buttons
function clickableDpadReleased() 
{
    gamerInput = new GamerInput("None");
}
function clickDpadYellow()
{
    gamerInput = new GamerInput("Up");
}
function clickDpadBlue()
{
    gamerInput = new GamerInput("Left");
}
function clickDpadRed()
{
    gamerInput = new GamerInput("Right");
}
function clickDpadGreen()
{
    gamerInput = new GamerInput("Down");
}
let yellowButton = document.getElementsByClassName("yellow")[0];
let blueButton = document.getElementsByClassName("blue")[0];
let redButton = document.getElementsByClassName("red")[0];
let greenButton = document.getElementsByClassName("green")[0];

function input(event)
{
        if (event.type === "keydown") {
        switch (event.keyCode) {
            case left_arrow: // Left Arrow // blue
                gamerInput = new GamerInput("Left");
                blueButton.classList.add("pressed");
                direction=2;
                break; //Left key
            case up_arrow: // Up Arrow // yellow
                yellowButton.classList.add("pressed");
                gamerInput = new GamerInput("Up");
                direction = 3;
                break; //Up key
            case right_arrow: // Right Arrow // red
                redButton.classList.add("pressed");    
                gamerInput = new GamerInput("Right");
                direction=1;
                break; //Right key
            case down_arrow: // Down Arrow // green
                greenButton.classList.add("pressed");
                gamerInput = new GamerInput("Down");
                direction = 4;
                break; //Down key
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        direction=0;
        gamerInput = new GamerInput("None");
        redButton.classList.remove("pressed");
        blueButton.classList.remove("pressed");
        yellowButton.classList.remove("pressed");
        greenButton.classList.remove("pressed");

    }
}

function movement()
{
    if (gamerInput.action === "Up") {
        player.y-=speed;
    } else if (gamerInput.action === "Down") {
        player.y += speed; // Move Player Down
    } else if (gamerInput.action === "Left") {
        player.x -= speed; // Move Player Left
    } else if (gamerInput.action === "Right")
    {
        direction=1;
        console.log("moving right");
        player.x +=speed;
    }
}

function animate()
{
        ctx.drawImage(item.spritesheet,
            0,0,item.width,item.height,item.x,item.y,item.scaleX,item.scaleY);
}


function collision(gameObject)
{
    let xcol= (hitBox.x + hitBox.width > gameObject.x && hitBox.x< gameObject.x + gameObject.width );
    let ycol = (hitBox.y + hitBox.height > gameObject.y && hitBox.y< gameObject.y + gameObject.height );
    return xcol && ycol;
}
function checkCollision()
{
    if(collision(item))
    {
        item.x = Math.random() * 600;
        item.y = Math.random() * 600;
        val+=10;
        if(val>=100)
        {
            val=100;
        }
        score+=1;
        console.log("Collision");
    }
    if(collision(slimeHB))
    {
        slime.x= -50;
        slime.y = Math.random() * 600;
        val-=10;
        if(val<=0)
        {
            val=0;
        }
    }
}

function updateHB()
{
    hitBox.x= player.x + 32;
    hitBox.y = player.y +48;

    slimeHB.x = slime.x + 16;
    slimeHB.y = slime.y + 16;
}
   
function draw()
{
    
    ctx.clearRect(0, 0, WIDTH, HEIGHT);
    animate();
    animateSlime();
    animateRight();
    
    ctx.fillStyle = "#000000";
    ctx.font = "30px Georgia";
    ctx.fillText("Score : " + score , 660,30);
    
    ctx.fillStyle="#D57A66";
    // Draw the background
    ctx.fillStyle = "#000000";
    ctx.fillRect(0, 0, healthWidth, healthHeight);

    //ctx.strokeRect(hitBox.x,hitBox.y,hitBox.width,hitBox.height);
    //ctx.strokeRect(slimeHB.x,slimeHB.y,slimeHB.width,slimeHB.height);

  // Draw the fill
    ctx.fillStyle = "#00FF00";
    var fillVal = Math.min(Math.max(val / max, 0), 1);
    ctx.fillRect(0, 0, fillVal * healthWidth, healthHeight);

}
function animateSlime()
{
    slimeTime++;
    if (slimeTime > 3)//7 is the speed at how fast they animate
    {
        slimeFrame++;
        if (slimeFrame > 21) //frame 21 is the last frame the robot is running
        {
            slimeFrame = 15; //15 is the first frame the robot is running
        }
        slimeTime = 0;
    }
    var col = slimeFrame % 5; //total cols is 5
    var row = 2; 

    ctx.drawImage(slime.spritesheet,
        col * slime.width,
        row * slime.width,
        slime.width,
        slime.height,
        slime.x,
        slime.y,
        slime.scaleX,
        slime.scaleY);
}

let row=0;
let col=0;
function animateRight()
{
    
    if(direction==0)
    {
       col=0;
       row=0;
    }
    if(direction==1)
    {
        walkRight();
    }
    if(direction==2)
    {
        walkLeft();
    }
    if(direction==3)
    {
        walkUp();
    }
    if(direction==4)
    {
        walkDown();
    }

    ctx.drawImage(player.spritesheet,
        col * player.width , //top left x
        row * player.height, //top left y
        player.width, //width
        player.height, //height
        player.x, //
        player.y,
        player.scaleX,
        player.scaleY);

}
function walkUp()
{
    playerFrameDown=0
    playerFrameLeft=59;
    playerFrameRight=0;
    playerTime++;
    if (playerTime > 6)//6 is the speed at how fast they animate
    {
        playerFrameUp++;
        if (playerFrameUp > 6) //frame 6 is the last frame the robot is running
        {
            playerFrameUp = 0; //0 is the first frame the robot is running
        }
        playerTime = 0;
    }
    col = playerFrameUp % 6; //total cols is 6
    row = 5; 
}
function walkDown()
{
    playerFrameUp=0
    playerFrameLeft=59;
    playerFrameRight=0;
    playerTime++;
    if (playerTime > 6)//6 is the speed at how fast they animate
    {
        playerFrameDown++;
        if (playerFrameDown > 5) //frame 5 is the last frame the robot is running
        {
            playerFrameDown = 0; //0 is the first frame the robot is running
        }
        playerTime = 0;
    }
    col = playerFrameDown % 6; //total cols is 6
    row = 3; 
}
function walkRight()
{
    playerFrameLeft=59;
    playerTime++;
    if (playerTime > 6)//7 is the speed at how fast they animate
    {
        playerFrameRight++;
        if (playerFrameRight > 5) //frame 5 is the last frame the robot is running
        {
            playerFrameRight = 0; //0 is the first frame the robot is running
        }
        playerTime = 0;
    }
    col = playerFrameRight % 6; //total cols is 6
    row = 4; 
}
function walkLeft()
{
    playerFrameRight=0;
    playerTime++;
    if (playerTime > 6)//6 is the speed at how fast they animate
    {
        playerFrameLeft--;
        if (playerFrameLeft < 55) //frame 55 is the last frame the robot is running
        {
            playerFrameLeft = 59; //59 is the first frame the robot is running
        }
        playerTime = 0;
    }
    col = playerFrameLeft % 12; //total cols is 12  -- going backwards
    console.log(playerFrameLeft);
    row = 4; 
}

function moveSlime()
{
    if(slimeFrame <20 && slimeFrame >16)
    {
    slime.x+=4;
    }
    if(slime.x > 1200)
    {
        slime.x = -50;
        slime.y = Math.random() * 600;
    }

}
function update()
{
    updateHB();
    movement();
    moveSlime();
    checkCollision(item);
}

function gameloop()
{
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

window.requestAnimationFrame(gameloop); //animate the player

window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);
